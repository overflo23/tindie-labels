#!/usr/bin/python3

import requests
import json
import sys
import os
import subprocess
import time

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont


user = "USERNAME"
apikey = "APIKEY"

#Find the descriptors of your printers like this: lpstat -v
label_printer="QL800"
document_printer="Brother_MFC_L3750CDW_series@BRNB422003F6BB9.local"

# --------------------- no need to touch things below here


baseurl = "https://www.tindie.com/api/v1/order/?shipped=false"
# If the tindie fetch does not wwork, you can open a socket with nc-l -p 8090 and see what the program sends
# The important line should read Authorization: ApiKey <USERNAME>:<APIKEY>
# baseurl = "http://localhost:8080/api/v1/"


def createLabel(address, order_no, phone):
    bg = Image.open("res/label.png")

    font = ImageFont.truetype("res/dyuthi.ttf", 60)
    address_label = Image.new("RGBA", (775, 390), (255, 255, 255))
    draw = ImageDraw.Draw(address_label)
    draw.text((0, 0), address, (0, 0, 0), font=font)
    bg.paste(address_label, (120, 90))

    font = ImageFont.truetype("res/dyuthi.ttf", 32)
    order_code = Image.new("RGBA", (200, 60), (255, 255, 255))
    draw = ImageDraw.Draw(order_code)
    draw.text((0, 0), order_no, (0, 0, 0), font=font)
    bg.paste(order_code, (120, 536 - 32))

    font = ImageFont.truetype("res/dyuthi.ttf", 32)
    fon = Image.new("RGBA", (600, 60), (255, 255, 255))
    draw = ImageDraw.Draw(fon)
    draw.text((0, 0), phone, (0, 0, 0), font=font)
    bg.paste(fon, (120, 420))

    bg.save("label_output.png")


def createSlip(address, order_no, items):
    bg = Image.open("res/orderlist.png")

    font = ImageFont.truetype("res/dyuthi.ttf", 60)

    address_label = Image.new("RGBA", (775, 390), (0, 0, 0, 0))
    draw = ImageDraw.Draw(address_label)
    draw.text((0, 0), address, (0, 0, 0), font=font)
    bg.paste(address_label, (153, 350))

    order_code = Image.new("RGBA", (1000, 200), (0, 0, 0, 0))
    draw = ImageDraw.Draw(order_code)
    draw.text((0, 0), order_no, (0, 0, 0), font=font)
    bg.paste(order_code, (400, 3400))

    offset = 0

    for i in items:
        itemline = Image.new("RGBA", (1000, 70), (0, 0, 0, 0))
        draw = ImageDraw.Draw(itemline)
        draw.text((0, 0), str(i["quantity"]), (0, 0, 0), font=font)
        draw.text((100, 0), i["product"], (0, 0, 0), font=font)
        draw.text((400, 0), i["options"], (0, 0, 0), font=font)
        bg.paste(itemline, (153, 900 + offset))
        offset += 300

    bg.save("slip.png")


def printLabelAndSlip():
    s = subprocess.run(
        ["lp", "-d", label_printer, "label_output.png"], capture_output=True, text=True
    )
    s2 = subprocess.run(
        ["lp", "-d", document_printer, "slip.png"],
        capture_output=True,
        text=True,
    )

    time.sleep(1)
    try:
        os.unlink("label_output.png")
        os.unlink("slip.png")
    except:
        return


def display_order_line(order):
    print(
        str("[" + str(order_counter) + "]  " + str(order["number"]))
        + "   "
        + order["shipping_name"]
        + "  "
        + str(order["total"])
    )


def generateAndPrintForOrder(order):
    date = order["date"]
    company = order["company_title"]
    discount_code = order["discount_code"]
    email = order["email"]
    message = order["message"]
    order_id = order["number"]
    phone = order["phone"]
    refunded = order["refunded"]
    shipped = order["shipped"]

    shipping_city = order["shipping_city"]
    shipping_country = order["shipping_country"]
    shipping_country_code = order["shipping_country_code"]
    shipping_instructions = order["shipping_instructions"]
    shipping_name = order["shipping_name"]
    shipping_postcode = order["shipping_postcode"]
    shipping_service = order["shipping_service"]
    shipping_state = order["shipping_state"]
    shipping_street = order["shipping_street"]

    total = order["total"]
    total_ccfee = order["total_ccfee"]
    total_discount = order["total_discount"]
    total_kickback = order["total_kickback"]
    total_seller = order["total_seller"]
    total_shipping = order["total_shipping"]
    total_subtotal = order["total_subtotal"]
    total_tindiefee = order["total_tindiefee"]

    tracking_code = order["tracking_code"]
    tracking_url = order["tracking_url"]

    if company:
        address = (
            company
            + "\nc/o "
            + shipping_name
            + "\n"
            + shipping_street
            + "\n"
            + shipping_postcode
            + " "
            + shipping_city
            + "\n"
            + shipping_country
            + " ("
            + shipping_country_code
            + ")"
        )

    else:
        address = (
            shipping_name
            + "\n"
            + shipping_street
            + "\n"
            + shipping_postcode
            + " "
            + shipping_city
            + "\n"
            + shipping_country
            + " ("
            + shipping_country_code
            + ")"
        )

    print("Creating Label and Slip for: T" + str(order_id))
    createLabel(address, "T" + str(order_id), phone)
    createSlip(address, "T" + str(order_id), order["items"])
    printLabelAndSlip()


def printAllLabels():
    global data
    for order in data["orders"]:
        generateAndPrintForOrder(order)


print("Welcome to the TINDIE API tool")
print("Trying to load data from tindie")


r = requests.get(baseurl, headers={"Authorization": "ApiKey " + user + ":" + apikey})

if r.reason != "OK":
    print("Aborting! Something went wrong.\r\n", r.reason)
    sys.exit()

data = json.loads(r.content.decode())

total_orders = data["meta"]["total_count"]

if total_orders < 1:
    print("There are NO open orders at the moment :( ")
    sys.exit(0)
elif total_orders == 1:
    print("There is exactly ONE open order at he moment. Printing it right away mam/sir/whatever!")
    generateAndPrintForOrder(data["orders"][0])
    sys.exit(0)


order_counter = 0


print("There are " + str(total_orders) + " open orders to deal with:\n")


print("No  (Tindie)  Name          Total")
print("---------------------------------")
for order in data["orders"]:
    display_order_line(order)
    order_counter += 1


try:
    o_id = int(
        input(
            "\n-----------------------------------\nInsert the No on the very left [1-"
            + str((order_counter))
            + "] or hit [ENTER] to print all orders > "
        ).strip()
    )
except:
    o_id = 0


if o_id > 0:
    try:
        print("Checking order [" + str(o_id) + "]")
        order = data["orders"][o_id - 1]
    except:
        print(":( No such order: " + str(o_id))
        sys.exit(1)

    # print(order)
    generateAndPrintForOrder(order)
else:
    print("\nPrinting ALL the labels \o/")
    printAllLabels()
