This is a small tool i hacked together for a friends tindie shop.

It fetches all open orders form tindie.

If there is more than one open order, you can select one or all to generate shipping labels and a packaging list.

If there is just one order it will automatically generate the files for that order and print them.

